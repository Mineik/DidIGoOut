package site.superprojekty.didigoout.Util.AsyncTask;

import android.location.Address;

import android.util.Log;
import org.osmdroid.util.GeoPoint;

import java.io.IOException;
import java.util.List;

public class District extends City{
	@Override
	protected List<Address> doInBackground(GeoPoint... geoPoints) {
		GeoPoint point = geoPoints[0];

		try{
			List<Address> adresy = super.nominatimAPI.getFromLocation(point.getLatitude(), point.getLongitude(),1);
			if(adresy.size() == 0) throw new IOException();
			return adresy;
		}catch(IOException e){
			return null;
		}
	}


	@Override
	protected void onPostExecute(List<Address> addresses){
		Log.d("DEBUG", "request Executed: District");
		city = addresses;
	}
}
