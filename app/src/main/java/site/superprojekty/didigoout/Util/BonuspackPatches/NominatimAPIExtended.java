package site.superprojekty.didigoout.Util.BonuspackPatches;

import android.location.Address;
import android.os.Bundle;
import android.util.Log;
import com.google.gson.*;
import org.json.JSONArray;
import org.osmdroid.bonuspack.location.GeocoderNominatim;
import org.osmdroid.bonuspack.utils.BonusPackHelper;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


/**
 * Collection of patches towards GeocoderNominatim class. Eventhough the class needs more attention, this fix for what I need
 *
 *
 * @see org.osmdroid.bonuspack.location.GeocoderNominatim
 * @see <a href="https://github.com/MKergall/osmbonuspack/blob/master/OSMBonusPack/src/main/java/org/osmdroid/bonuspack/location/GeocoderNominatim.java">The original source code on github</a>
 */
public class NominatimAPIExtended extends GeocoderNominatim {

	public NominatimAPIExtended(String userAgent) {
		super(userAgent);
	}


	public List<Address> getPolyFromName(String locationName, int maxResults) throws IOException {
		String url = mServiceUrl + "search.php?";
		if (mKey != null)
			url += "key=" + mKey + "&";
		url += "format=json"
				+ "&accept-language=" + mLocale.getLanguage()
				+ "&addressdetails=1"
				+ "&limit=" + maxResults
				+ "&q=" + URLEncoder.encode(locationName);
		if (mPolygon){
			//get polygon outlines for items found:
			url += "&polygon_geojson=1";
			//Upgrade is on hold, waiting for MapQuest service to become compatible.
		}
		Log.d(BonusPackHelper.LOG_TAG, "GeocoderNominatim::getFromLocationName:"+url);
		String result = BonusPackHelper.requestStringFromUrl(url, mUserAgent);
		//Log.d(BonusPackHelper.LOG_TAG, result);
		if (result == null)
			throw new IOException();
		try {
			JsonParser parser = new JsonParser();
			JsonElement json = parser.parse(result);
			JsonArray jResults = json.getAsJsonArray();
			List<Address> list = new ArrayList<Address>(jResults.size());
			for (int i=0; i<jResults.size(); i++){
				JsonObject jResult = jResults.get(i).getAsJsonObject();
				Address gAddress = buildAndroidAddress(jResult);
				if (gAddress != null)
					list.add(gAddress);
			}
			//Log.d(BonusPackHelper.LOG_TAG, "done");
			return list;
		} catch (JsonSyntaxException e) {
			throw new IOException();
		}
	}


	/**
	 * Build an Android Address object from the Nominatim address in JSON format.
	 * Current implementation is mainly targeting french addresses,
	 * and will be quite basic on other countries.
	 * @return Android Address, or null if input is not valid.
	 */
	@Override
	protected Address buildAndroidAddress(JsonObject jResult) throws JsonSyntaxException{
		Address gAddress = new Address(mLocale);
		if (!jResult.has("lat") || !jResult.has("lon") || !jResult.has("address"))
			return null;

		gAddress.setLatitude(jResult.get("lat").getAsDouble());
		gAddress.setLongitude(jResult.get("lon").getAsDouble());
		JsonObject jAddress = jResult.get("address").getAsJsonObject();

		int addressIndex = 0;
		if (jAddress.has("road")){
			gAddress.setAddressLine(addressIndex++, jAddress.get("road").getAsString());
			gAddress.setThoroughfare(jAddress.get("road").getAsString());
		}

		if (jAddress.has("house_number")){
			gAddress.setSubThoroughfare(jAddress.get("house_number").getAsString());
		}

		if (jAddress.has("suburb")){
			//gAddress.setAddressLine(addressIndex++, jAddress.getString("suburb"));
			//not kept => often introduce "noise" in the address.
			gAddress.setSubLocality(jAddress.get("suburb").getAsString());
		}

		if (jAddress.has("postcode")){
			gAddress.setAddressLine(addressIndex++, jAddress.get("postcode").getAsString());
			gAddress.setPostalCode(jAddress.get("postcode").getAsString());
		}

		if (jAddress.has("city")){
			gAddress.setAddressLine(addressIndex++, jAddress.get("city").getAsString());
			gAddress.setLocality(jAddress.get("city").getAsString());
		} else if (jAddress.has("town")){
			gAddress.setAddressLine(addressIndex++, jAddress.get("town").getAsString());
			gAddress.setLocality(jAddress.get("town").getAsString());
		} else if (jAddress.has("village")){
			gAddress.setAddressLine(addressIndex++, jAddress.get("village").getAsString());
			gAddress.setLocality(jAddress.get("village").getAsString());
		}

		if (jAddress.has("county")){ //France: departement
			gAddress.setSubAdminArea(jAddress.get("county").getAsString());
		}
		if (jAddress.has("state")){ //France: region
			gAddress.setAdminArea(jAddress.get("state").getAsString());
		}
		if (jAddress.has("country")){
			gAddress.setAddressLine(addressIndex++, jAddress.get("country").getAsString());
			gAddress.setCountryName(jAddress.get("country").getAsString());
		}
		if (jAddress.has("country_code"))
			gAddress.setCountryCode(jAddress.get("country_code").getAsString());

		/* Other possible OSM tags in Nominatim results not handled yet:
		 * subway, golf_course, bus_stop, parking,...
		 * house, house_number, building
		 * city_district (13e Arrondissement)
		 * road => or highway, ...
		 * sub-city (like suburb) => locality, isolated_dwelling, hamlet ...
		 * state_district
		 */

		//Add non-standard (but very useful) information in Extras bundle:
		Bundle extras = new Bundle();
		if (jResult.has("polygonpoints")){
			JsonArray jPolygonPoints = jResult.get("polygonpoints").getAsJsonArray();
			ArrayList<GeoPoint> polygonPoints = new ArrayList<GeoPoint>(jPolygonPoints.size());
			for (int i=0; i<jPolygonPoints.size(); i++){
				JsonArray jCoords = jPolygonPoints.get(i).getAsJsonArray();
				double lon = jCoords.get(0).getAsDouble();
				double lat = jCoords.get(1).getAsDouble();
				GeoPoint p = new GeoPoint(lat, lon);
				polygonPoints.add(p);
			}
			extras.putParcelableArrayList("polygonpoints", polygonPoints);
		}else if(jResult.has("geojson")){
			JsonArray jPolygonPoints = jResult.get("geojson").getAsJsonObject().getAsJsonArray("coordinates").get(0).getAsJsonArray();
			ArrayList<GeoPoint> polygonPoints = new ArrayList<GeoPoint>(jPolygonPoints.size());
			for (int i=0; i<jPolygonPoints.size(); i++){
				JsonArray jCoords = jPolygonPoints.get(i).getAsJsonArray();
				double lon = jCoords.get(0).getAsDouble();
				double lat = jCoords.get(1).getAsDouble();
				GeoPoint p = new GeoPoint(lat, lon);
				polygonPoints.add(p);
			}
			try{
				extras.putParcelableArrayList("polygonpoints", polygonPoints);
			}catch(Exception e){
				Log.d("DEBUG", "EXC");
			}
		}

		if (jResult.has("boundingbox")){
			JsonArray jBoundingBox = jResult.get("boundingbox").getAsJsonArray();
			BoundingBox bb = new BoundingBox(
					jBoundingBox.get(1).getAsDouble(), jBoundingBox.get(2).getAsDouble(),
					jBoundingBox.get(0).getAsDouble(), jBoundingBox.get(3).getAsDouble());
			extras.putParcelable("boundingbox", bb);
		}
		if (jResult.has("osm_id")){
			long osm_id = jResult.get("osm_id").getAsLong();
			extras.putLong("osm_id", osm_id);
		}
		if (jResult.has("osm_type")){
			String osm_type = jResult.get("osm_type").getAsString();
			extras.putString("osm_type", osm_type);
		}
		if (jResult.has("display_name")){
			String display_name = jResult.get("display_name").getAsString();
			extras.putString("display_name", display_name);
		}
		gAddress.setExtras(extras);

		return gAddress;
	}

}
