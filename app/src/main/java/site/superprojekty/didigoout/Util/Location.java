package site.superprojekty.didigoout.Util;

import android.location.Address;
import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;
import org.osmdroid.util.GeoPoint;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import site.superprojekty.didigoout.Util.AsyncTask.City;
import site.superprojekty.didigoout.Util.AsyncTask.District;
import site.superprojekty.didigoout.Util.AsyncTask.GetGeoPointsFromName;

public class Location {


	public static String getMyCurrentCity(int CITY_ADMINISTRATIVE_LEVEL, GeoPoint myCurrentLocation){
		try{
			 City mestoFetcher = new City();
			 List<Address> adresy = mestoFetcher.execute(myCurrentLocation).get();

			if(adresy.size() == 0 || adresy.get(0).getAdminArea() == null) throw new IOException();
			else{
				Log.d("DEBUG", adresy.get(0).getLocality());
				return adresy.get(0).getLocality();
			}
		}
		catch(Exception exc){
			Log.d("DEBUG", exc.getMessage());
			return "";
		}
	}

	public static String getMyCurrentCity(GeoPoint myCurrentLocation){
		return getMyCurrentCity(8, myCurrentLocation);
	}


	public static List<GeoPoint> getPlacePolygon(String placeName, int ADMINISTRATIVE_LEVEL) throws ExecutionException, InterruptedException {
		return new GetGeoPointsFromName().execute(placeName).get();
	}
//
//	public static List<GeoPoint> getCityPolygon(String cityName){
//		return getPlacePolygon(cityName, 8);
//	}
//
//	public static List<GeoPoint> getDistrictPolygon(String districtName){
//		return getPlacePolygon(districtName, 6);
//	}

	/**
	 * Gets district name from adress extras
	 * @param currentLocation location of user
	 * @return name of disctrict found in the displayed adress
	 */
	@RequiresApi(api = Build.VERSION_CODES.N)
	public static String getMyCurrentDistrict(GeoPoint currentLocation){
		try{
		District districts = new District();
		List<Address> adresy =  districts.execute(currentLocation).get();
		if(adresy.size() == 0) throw new IOException();
		else{
			String fullAdress = (String) adresy.get(0).getExtras().get("display_name");
			assert fullAdress != null;
			String district = "";
			for(String element: fullAdress.split(", ")){
				if(element.startsWith("okres")){
					district = element.replace("okres ", "");
					break;
				}
			}
			Log.d("DEBUG", district);
			return district;
		}
		}catch(Exception e){
			Log.d("DEBUG", e.getMessage());
			return "";
		}
	}




}
