package site.superprojekty.didigoout.Util.AsyncTask;

import android.location.Address;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import site.superprojekty.didigoout.Util.BonuspackPatches.NominatimAPIExtended;

import java.util.ArrayList;
import java.util.List;

public class GetGeoPointsFromName extends AsyncTask<String, Void, List<GeoPoint>> {
	NominatimAPIExtended nominatim = new NominatimAPIExtended(Configuration.getInstance().getUserAgentValue());
	@Override
	protected List<GeoPoint> doInBackground(String... strings){
		nominatim.setOptions(true);
		try {
			List<Address> adressList = nominatim.getPolyFromName(strings[0],1);
			Address adresa = adressList.get(0);

			return (List<GeoPoint>) adresa.getExtras().get("polygonpoints");
		} catch (Exception e) {
			Log.d("DEBUG","exc tady");
			e.printStackTrace();
		}

		return null;
	}
}
