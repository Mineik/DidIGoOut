package site.superprojekty.didigoout.Util.Cacher;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.osmdroid.util.GeoPoint;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

public class LocalConfig {
	private Dictionary<String, Dictionary<String, List<GeoPoint>>> configStore;
	private SharedPreferences prefs;
	private Gson gson;
	public LocalConfig(Context context){
		gson = new GsonBuilder().create();
		this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
	}


	public void setConfigItem(String item, String value){
		prefs.edit().putString(item, value);
	}

	public String getConfigItem (String item){
		return prefs.getString(item, "");
	}

	public void setCityPreference(String cityName, List<GeoPoint> points){
		Dictionary<String, List<GeoPoint>> cityStore = null;
		cityStore.put(cityName, points);
		configStore.put("city", cityStore);

		StorerObject object = this.handleSavingStuff(cityName, points);

		this.setConfigItem("city", gson.toJson(object));
	}

	public void setDistrictPreference(String cityName, List<GeoPoint> points){
		Dictionary<String, List<GeoPoint>> cityStore = null;
		cityStore.put(cityName, points);
		configStore.put("district", cityStore);

		StorerObject object = this.handleSavingStuff(cityName, points);

		this.setConfigItem("district", gson.toJson(object));
	}

	private StorerObject handleSavingStuff(String cityName, List<GeoPoint> points) {
		List<List<Double>> pointsList= new ArrayList<>();

		for(GeoPoint point: points){
			List<Double> pointList = new ArrayList<>();
			pointList.add(point.getLatitude());
			pointList.add(point.getLongitude());
			pointsList.add(pointList);
		}

		StorerObject object = new StorerObject();

		object.name = cityName;
		object.coordinates = pointsList;

		return object;
	}


	public StorerObject getSetDistrict(){
		return gson.fromJson(this.getConfigItem("district"), (Type) StorerObject.class);
	}

	public StorerObject getSetCity(){
		return gson.fromJson(this.getConfigItem("city"), (Type) StorerObject.class);
	}
}
