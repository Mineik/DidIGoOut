package site.superprojekty.didigoout.Util.AsyncTask;

import android.location.Address;
import android.os.AsyncTask;
import android.util.Log;
import org.osmdroid.bonuspack.location.GeocoderNominatim;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;

import java.io.IOException;
import java.util.List;

public class City extends AsyncTask<GeoPoint, Void, List<Address>> {
	protected GeocoderNominatim nominatimAPI = new GeocoderNominatim(Configuration.getInstance().getUserAgentValue());
	public List<Address> city;

	@Override
	protected List<Address> doInBackground(GeoPoint... geoPoints) {
		GeoPoint point = geoPoints[0];


		try {
			return nominatimAPI.getFromLocation(point.getLatitude(), point.getLongitude(),0);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(List<Address> addresses){
		Log.d("DEBUG", "request Executed: City");
		this.city = addresses;
	}



}
