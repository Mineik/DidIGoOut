package site.superprojekty.didigoout;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import com.google.gson.Gson;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Polygon;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static site.superprojekty.didigoout.Util.Location.*;

public class MainActivity extends AppCompatActivity implements LocationListener {
	private final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
	private MapView map = null;
	private MyLocationNewOverlay mLocationOverlay;
	private Location location;
	private IMapController mapController;
	private Context ctx;
	private SharedPreferences preferences;
	private boolean hasCityOverlayBeenRenderedYet = false;
	private boolean hasDistrictOverlayBeenRenderedYet = false;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
		Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
        super.onCreate(savedInstanceState);
		ctx = getApplicationContext();
		Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
		preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		requestPermissionsIfNecessary(new String[] {
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.ACCESS_COARSE_LOCATION,
				Manifest.permission.WRITE_EXTERNAL_STORAGE
		});
        setContentView(R.layout.activity_main);


        //create map renderer
		map = (MapView) findViewById(R.id.map);
		map.setTileSource(TileSourceFactory.MAPNIK);
		GpsMyLocationProvider gpsMyLocationProvider = new GpsMyLocationProvider(ctx);
		gpsMyLocationProvider.addLocationSource(LocationManager.GPS_PROVIDER);
		this.mLocationOverlay = new MyLocationNewOverlay(gpsMyLocationProvider,map);
		this.mLocationOverlay.enableMyLocation();
		this.mLocationOverlay.enableFollowLocation();


		//setup map controller
		mapController =  map.getController();
		mapController.setZoom(13);

		map.setBuiltInZoomControls(false);
		map.setMultiTouchControls(true);

		//update to my location
		map.getOverlays().add(this.mLocationOverlay);
		GeoPoint startPoint = mLocationOverlay.getMyLocation();
		mapController.setCenter(startPoint);

		//start recieving location updates
		try{
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 1, this);
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0, 1, this);
		}
		catch(NullPointerException exception){
			Log.d("DEBUG","Caught NullPointerException");
		}catch(Exception e){
			Log.d("DEBUG", e.getMessage());
		}
    }


	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		ArrayList<String> permissionsToRequest = new ArrayList<>();
		for (int i = 0; i < grantResults.length; i++) {
			permissionsToRequest.add(permissions[i]);
		}
		if (permissionsToRequest.size() > 0) {
			ActivityCompat.requestPermissions(
					this,
					permissionsToRequest.toArray(new String[0]),
					REQUEST_PERMISSIONS_REQUEST_CODE);
		}
	}


	@Override
	public void onLocationChanged(Location kokot){
		Log.d("SUCC", "Poloha se updatnula");
		this.location = kokot;
		GeoPoint myPoint = new GeoPoint(kokot.getLatitude(), kokot.getLongitude());
		mapController.setCenter(myPoint);
		notifyonUserBS(myPoint);
		map.invalidate();


		if(!hasCityOverlayBeenRenderedYet)renderCityOverlay(getMyCurrentCity(myPoint)); //lag hlavnihoUI go brrr
		if(!hasDistrictOverlayBeenRenderedYet) renderDistrictOverlay(getMyCurrentDistrict(myPoint)); //lag hlavního UI go ještě víc brrr
	}

	private void notifyonUserBS(GeoPoint startPoint) {
		Log.d("DEBUG", String.valueOf(startPoint.getLatitude()));
		Log.d("DEBUG", String.valueOf(startPoint.getLongitude()));
		try{
			createNotificationChannel("DIDILEAVE_DEBUG", "Debug notif", "Test notif");
			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.ctx, "DIDILEAVE_DEBUG")
					.setSmallIcon(R.drawable.ic_launcher_foreground)
					.setContentTitle("Your District and city")
					.setContentText(String.format("City: %s District: %s", getMyCurrentCity(startPoint), getMyCurrentDistrict(startPoint)));

			NotificationManagerCompat.from(this.ctx).notify(new Random().nextInt(69421), notificationBuilder.build());
			Log.d("DEBUG", "notification sent!");
		}catch (Exception e){
			Log.d("DEBUG", String.format("Caught %s", e.getMessage()));
			Log.e("DEBUG", String.format("Caught %s", e.getMessage()));
			e.printStackTrace();
		}
	}

	public void onStatusChanged(String provide, int status, Bundle extras){}
	public void onProviderEnabled(String provide){
		Log.d("DEBUG", "Provider spuštěn");
	}
	public void onProviderDisabled(String provide){
		Log.d("DEBUG", "Provider vypnut");
	}

	private void requestPermissionsIfNecessary(String[] permissions) {
		ArrayList<String> permissionsToRequest = new ArrayList<>();
		for (String permission : permissions) {
			if (ContextCompat.checkSelfPermission(this, permission)
					!= PackageManager.PERMISSION_GRANTED) {
				// Permission is not granted
				permissionsToRequest.add(permission);
			}
		}
		if (permissionsToRequest.size() > 0) {
			ActivityCompat.requestPermissions(
					this,
					permissionsToRequest.toArray(new String[0]),
					REQUEST_PERMISSIONS_REQUEST_CODE);
		}
	}

	private void createNotificationChannel(String channelId, String channelName, String channelDescription) {
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is new and not in the support library
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
			channel.setDescription(channelDescription);
			// Register the channel with the system; you can't change the importance
			// or other notification behaviors after this
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
	}

	private void renderCityOverlay(String cityName){
		if(hasCityOverlayBeenRenderedYet) return; //just basic check if someone hadn't fuck up
		try{
			Polygon cityOverlay = new Polygon();
			List<GeoPoint> cityPoints = getPlacePolygon(cityName, 8);
			for (GeoPoint point:cityPoints) { cityOverlay.addPoint(point); }
			cityOverlay.getOutlinePaint().setColor(ContextCompat.getColor(ctx, R.color.colorAccent));
			cityOverlay.getFillPaint().setColor(ContextCompat.getColor(ctx, R.color.colorAccent));
			cityOverlay.getFillPaint().setAlpha(50);

			map.getOverlays().add(cityOverlay);
			hasCityOverlayBeenRenderedYet = true;
		}catch(Exception e){
			Log.d("DEBUG", e.getMessage());
			e.printStackTrace();
		}
	}

	private void renderDistrictOverlay(String districtName){
		if(hasDistrictOverlayBeenRenderedYet) return; //just basic check if someone hadn't fuck up
		try{
			Polygon districtOverlay = new Polygon();
			List<GeoPoint> cityPoints = getPlacePolygon("okres "+districtName, 6);
			for (GeoPoint point:cityPoints) { districtOverlay.addPoint(point); }
			districtOverlay.getOutlinePaint().setColor(ContextCompat.getColor(ctx, R.color.colorPrimaryDark));
			districtOverlay.getFillPaint().setColor(ContextCompat.getColor(ctx, R.color.colorPrimaryDark));
			districtOverlay.getFillPaint().setAlpha(50);

			map.getOverlays().add(districtOverlay);
			hasDistrictOverlayBeenRenderedYet = true;
		}catch(Exception e){
			Log.d("DEBUG", e.getMessage());
			e.printStackTrace();
		}
	}
}